<?php if ( $display ): ?>

 	<div class="row">
 		<div class="vimeo-player col-12 col-md-6 col-lg-9">
 			<h4>Skicka in klipp för feedback</h4>
      <?php
        echo do_shortcode('[forminator_form id="8300"]');

        // Lista personens köpta klipp för oss som hanterar individen så att vi vet vad de har tillgång till för info!
      ?>
 		</div>

 		<div class="spotify-list col-12 col-md-6 col-lg-3">
 			<h4>Vårt svar</h4>

 		</div>
 	</div>

<?php endif; ?>

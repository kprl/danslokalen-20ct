<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php

	get_template_part( 'template-parts/entry-header-alt' );

	if ( ! is_search() ) {
		//get_template_part( 'template-parts/featured-image' );
	}

	$display 	= false;
	$meta 		= get_post_meta(get_the_ID());
	$template = "undervisning-none";

	if ( Woocommerce_Pay_Per_Post_Helper::has_access() ):

		$display = true;

		$undervisning = $meta['typ_av_undervisning'][0];

		if ( $undervisning == "feedback" ) {
			$template = "undervisning-feedback";
		} else if ( $undervisning == "videoklipp" ) {
			$template = "undervisning-videoklipp";
			$embed 	 = str_replace("vimeo.com/", "player.vimeo.com/video/", $meta['videoklipp_vimeo'][0]);
			$spotify = embed_spotify_link( $meta['videoklipp_spotify'][0] );
		} else {
			$template = "content";
		}

	endif;

	?>

	<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">

		<div class="entry-content">

			<div class="row">
				<div class="col-12">
					<?php
						if ( is_search() || ! is_singular() && 'summary' === get_theme_mod( 'blog_content', 'full' ) ) {
							the_excerpt();
						} else {
							the_content( __( 'Continue reading', 'twentytwenty' ) );
						}

						$product = wc_get_product( $meta['wc_pay_per_post_product_ids'][0] );
						echo wpautop( $product->get_description() );
					?>
				</div>
			</div>

			<?php
				if ( $display ) {
					include( get_stylesheet_directory() . '/template-parts/' . $template . '.php' );
				}
			?>

		</div><!-- .entry-content -->

	</div><!-- .post-inner -->

	<div class="section-inner">
		<?php
		wp_link_pages(
			array(
				'before'      => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'twentytwenty' ) . '"><span class="label">' . __( 'Pages:', 'twentytwenty' ) . '</span>',
				'after'       => '</nav>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			)
		);

		edit_post_link();

		// Single bottom post meta.
		twentytwenty_the_post_meta( get_the_ID(), 'single-bottom' );

		if ( is_single() ) {

			//get_template_part( 'template-parts/entry-author-bio' );

		}
		?>

	</div><!-- .section-inner -->

	<?php

	if ( is_single() ) {

		get_template_part( 'template-parts/navigation' );

	}

	/**
	 *  Output comments wrapper if it's a post, or if comments are open,
	 * or if there's a comment number – and check for password.
	 * */
	if ( ( is_single() || is_page() ) && ( comments_open() || get_comments_number() ) && ! post_password_required() ) {
		?>

		<div class="comments-wrapper section-inner">

			<?php comments_template(); ?>

		</div><!-- .comments-wrapper -->

		<?php
	}
	?>

</article><!-- .post -->

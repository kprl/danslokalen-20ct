<?php if ( $display ): ?>

 	<div class="row">
 		<div class="vimeo-player col-12 col-md-6 col-lg-9">
 			<h4>Övningsmaterial</h4>
 			<?php
 				if ( $embed ) {
 					echo '<iframe src="' . $embed . '" width="580" height="326" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen data-origwidth="580" data-origheight="326" style="width: 580px; height: 326px;"></iframe>';
 				}
 			?>
 		</div>

 		<div class="spotify-list col-12 col-md-6 col-lg-3">
 			<h4>Låtar i detta klipp</h4>
 			<?php
 				if ( $spotify ) {
 					echo '<iframe src="' . $spotify . '" style="padding-top: 8px;" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>';
 				}
 			?>
 		</div>
 	</div>

 	<?php $commentsection = false; //to come... ?>

 	<?php if ( $commentsection ): ?>

 	<div class="row">
 		<div class="add-comments col-12 col-md-6 col-lg-4">
 			<h4>Lägg till kommentar</h4>
 			<?php
 				echo do_shortcode('[forminator_form id="8178"]');
 			?>
 		</div>

 		<div class="user-comments col-12 col-md-6 col-lg-8">
 			<h4>Dina kommentarer</h4>

 			<?php

        $args = array(
          'author'        	=> get_current_user_id(),
          'orderby'       	=> 'post_date',
          'order'         	=> 'DESC',
          'posts_per_page' 	=> -1,
          'post_type' 			=> 'post',
        );

        foreach (get_posts( $args ) as $key => $post) {
          echo '<div class="alert alert-secondary" role="alert">' . wpautop($post->post_content) . '</div>';
        }

 			?>

 		</div>
 	</div>

 	<?php endif; ?>

<?php endif; ?>

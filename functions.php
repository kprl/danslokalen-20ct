<?php

date_default_timezone_set('Europe/Stockholm');
setlocale(LC_TIME,'sv_SE');

// Enqueue parent theme styles and child theme stylesheet
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

// Enqueue admin styles and stylesheet
function admin_style() {
  wp_enqueue_style('admin-styles', get_stylesheet_directory_uri() . '/css/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');

if ( ! function_exists( 'pre_r' ) ) {
  function pre_r( $val, $die = 0 ) {
    echo "<pre>";
    print_r( $val );
    echo "</pre>";
    if ( $die == 1 ) { die(); }
  }
}

/*** För en gemensam menykategori i adminlägets vänsterspalt. ***/
function danslokalen_add_admin_menu() {

  add_menu_page(
    __('Danslokalen', 'twentytwenty'),
    __('Danslokalen', 'twentytwenty'),
    'manage_options',
    'danslokalen-options',
    '',
    get_stylesheet_directory_uri() . '/danslokalen_logo_ul_sv_16x16.png',
    3
    );

	//Inställningar och information
  add_submenu_page(
    'danslokalen-options',
    __('Inställningar', 'twentytwenty'),
    __('Inställningar', 'twentytwenty'),
    'manage_options',
    'danslokalen-options',
    'danslokalen_options_page'
    );

}
add_action( 'admin_menu', 'danslokalen_add_admin_menu' );

// Including CPT:s
require_once('cpt/danslokalen_dansband.php');
require_once('cpt/danslokalen_underv.php');
require_once('cpt/danslokalen_feedback.php');

function danslokalen_acf_init() {

  /** BLOCKS **/
  // check function exists
  if ( function_exists('acf_register_block') ) {

    acf_register_block(array(
      'name'            => 'danslokalen-dansband',
      'title'           => __('Danslokalen Dansband'),
      'description'		  => __('Block för dansband.'),
      'render_template' => get_stylesheet_directory() . '/blocks/block-danslokalen_dansband.php',
      'mode'            => 'preview',
      'align'           => 'wide',
      'category'        => 'layout',
      'icon'            => 'vault',
      'keywords'        => array( 'Danslokalen', 'Dansband', 'kprl' ),
    ));

		acf_register_block(array(
      'name'            => 'danslokalen-underv',
      'title'           => __('Danslokalen Köpt material'),
      'description'		  => __('Block för köpt material.'),
      'render_template' => get_stylesheet_directory() . '/blocks/block-danslokalen_underv.php',
      'mode'            => 'preview',
      'align'           => 'wide',
      'category'        => 'layout',
      'icon'            => 'vault',
      'keywords'        => array( 'Danslokalen', 'Undervisning', 'Köpt material', 'kprl' ),
    ));

  }

}
add_action('acf/init', 'danslokalen_acf_init');



/**
 * Disables repeat purchase for products / variations
 *
 * @param bool $purchasable true if product can be purchased
 * @param \WC_Product $product the WooCommerce product
 * @return bool $purchasable the updated is_purchasable check
 */
function sv_disable_repeat_purchase( $purchasable, $product ) {

	// Don't run on parents of variations,
	// function will already check variations separately
	if ( $product->is_type( 'variable' ) ) {
		return $purchasable;
	}

	// Get the ID for the current product (passed in)
	$product_id = $product->get_id();

  // return false if the customer has bought the product / variation AND if videoklipp
  if ( wc_customer_bought_product( wp_get_current_user()->user_email, get_current_user_id(), $product_id ) ) {
		$pids = get_danslokalen_underv_by_custom_id($product_id);
		$meta = get_post_meta($pids[0]->ID, 'typ_av_undervisning');
		if ( $meta[0] == "videoklipp" ) {
			$purchasable = false;
		} else if ( $meta[0] == "feedback" ) {
			$uids = get_danslokalen_feedback_by_custom_id(get_current_user_id());
			$purchasable = true;
			foreach ($uids as $key => $uid) {
				$status = get_post_meta( $uid->ID, 'danslokalen_feedback_status' );
				if ( $status[0] == 1 ) {
					$purchasable = false;
				}
			}
		} else {
			$purchasable = true;
		}

  }

  return $purchasable;
}
add_filter( 'woocommerce_is_purchasable', 'sv_disable_repeat_purchase', 10, 2 );


/**
 * Shows a "purchase disabled" message to the customer
 */
function sv_purchase_disabled_message() {
	global $product; // get the current product to see if it has been purchased

	if ( $product->is_type( 'variable' ) ) {

		foreach ( $product->get_children() as $variation_id ) {
			// Render the purchase restricted message if it has been purchased
			if ( wc_customer_bought_product( wp_get_current_user()->user_email, get_current_user_id(), $variation_id ) ) {
				sv_render_variation_non_purchasable_message( $product, $variation_id );
			}
		}

	} else {

		// Get the ID for the current product (passed in)
		$product_id = $product->get_id();

		if ( wc_customer_bought_product( wp_get_current_user()->user_email, get_current_user_id(), $product_id ) ) {

			$pids = get_danslokalen_underv_by_custom_id($product_id);
			$meta = get_post_meta($pids[0]->ID, 'typ_av_undervisning');

			if ( $meta[0] == "videoklipp" ) {
				echo '<div class="woocommerce">';
					echo '<div style="margin: 20px 0 0 0;" class="woocommerce-info wc-nonpurchasable-message">Du har redan köpt detta material en gång!</div>';
				echo '</div>';
				$pids = get_danslokalen_underv_by_custom_id($product->get_id());
				if ( wp_redirect( get_permalink( $pids[0]->ID ) ) ) {
					exit;
				}

				//echo("<script>location.href = '". get_the_permalink( $pids[0]->ID ) ."'</script>");
			} else if ( $meta[0] == "feedback" ) {
				$uids = get_danslokalen_feedback_by_custom_id(get_current_user_id());
				$hanterade = array();
				foreach ($uids as $uid) {
					$status = get_post_meta( $uid->ID, 'danslokalen_feedback_status' );
					if ( $status[0] == 1 ) {
						echo '<div class="woocommerce">';
							echo '<div style="margin: 20px 0 0 0;" class="woocommerce-info wc-nonpurchasable-message">Du har redan köpt detta material en gång!</div>';
						echo '</div>';
					} else {
						$hanterade[] = $uid->ID;
					}
				}

				if ( array_key_exists( 0, $hanterade ) ) {
					echo '<div class="woocommerce">';
						echo '<div style="margin: 20px 0 0 0;" class="woocommerce-info wc-nonpurchasable-message">Dina tidigare feedbacks: ';
						foreach ($hanterade as $h) {
							 echo $h . ", ";
						}
						echo '</div>';
					echo '</div>';
				}

			}

		}
	}
}
add_action( 'woocommerce_single_product_summary', 'sv_purchase_disabled_message', 31 );


/**
 * Generates a "purchase disabled" message to the customer for specific variations
 *
 * @param \WC_Product $product the WooCommerce product
 * @param int $no_repeats_id the id of the non-purchasable product
 */
function sv_render_variation_non_purchasable_message( $product, $no_repeats_id ) {

	// Double-check we're looking at a variable product
	if ( $product->is_type( 'variable' ) && $product->has_child() ) {

		$variation_purchasable = true;

		foreach ( $product->get_available_variations() as $variation ) {

			// only show this message for non-purchasable variations matching our ID
			if ( $no_repeats_id === $variation['variation_id'] ) {
				$variation_purchasable = false;
				echo '<div class="woocommerce"><div class="woocommerce-info wc-nonpurchasable-message js-variation-' . sanitize_html_class( $variation['variation_id'] ) . '">Du har redan köpt detta material en gång!</div></div>';
			}
		}
	}

	if ( ! $variation_purchasable ) {
		wc_enqueue_js("
			jQuery('.variations_form')
				.on( 'woocommerce_variation_select_change', function( event ) {
					jQuery('.wc-nonpurchasable-message').hide();
				})
				.on( 'found_variation', function( event, variation ) {
					jQuery('.wc-nonpurchasable-message').hide();
					if ( ! variation.is_purchasable ) {
						jQuery( '.wc-nonpurchasable-message.js-variation-' + variation.variation_id ).show();
					}
				})
			.find( '.variations select' ).change();
		");
	}
}

add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_add_cart_button_single_product' );
function woocommerce_custom_add_cart_button_single_product( $label ) {
	if ( !is_admin() ) {
		foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
			$product = $values['data'];
			if( get_the_ID() == $product->get_id() ) {
				$label = __('Redan tillagd i varukorgen', 'woocommerce');
			}
		}
		return $label;
	}
}

add_filter( 'woocommerce_product_add_to_cart_text', 'woocommerce_custom_add_cart_button_loop', 99, 2 );
function woocommerce_custom_add_cart_button_loop( $label, $product ) {
	if ( $product->is_purchasable() && $product->is_in_stock() && !is_admin() ) {
		foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
			$_product = $values['data'];
			if( $product->get_id() == $_product->get_id() ) {
				$label = __('Redan i varukorg', 'woocommerce');
			}
		}
	}

	if ( !$product->is_in_stock() ) {
		$label = __('Kommer snart', 'woocommerce');
	}

	if ( wc_customer_bought_product( wp_get_current_user()->user_email, get_current_user_id(), $product->get_id() ) AND !$product->is_purchasable() ) {
		$taglist = wc_get_product_tag_list($product->get_id());
		$underv = get_danslokalen_underv_by_custom_id( $product->get_id() );
		$underm = get_post_meta($underv[0]->ID, 'undervisning_titel');
		if ( $underm[0] ) {
			$label = __('Visa', 'woocommerce') . " " . strip_tags($underm[0]);
		} else {
			$label = __('Visa', 'woocommerce');
		}

	}

	if ( 'Read more' == $label ) {
		$label = __( 'Läs mer', 'woocommerce' );
	} else if ( 'Show' == $label ) {
		$label = __( 'Visa', 'woocommerce' );
	}

	return $label;
}

add_filter( 'woocommerce_get_availability', 'custom_get_availability', 1, 2);

function custom_get_availability( $availability, $_product ) {
	if ( !$_product->is_in_stock() ) $availability['availability'] = __('Kommer snart!', 'woocommerce');
	return $availability;
}

function get_danslokalen_underv_by_custom_id($id = array(), $field = 'wc_pay_per_post_product_ids') {

	$post_ids = get_posts( array(
		'post_type'			=> 'danslokalen_underv',
    'meta_key'   		=> $field,
    'meta_value' 		=> $id,
    'meta_compare' 	=> 'LIKE',
	) );

	return $post_ids;

}

function get_danslokalen_feedback_by_custom_id($id = null, $field = 'danslokalen_feedback_userid') {

	$post_ids = get_posts( array(
		'post_type'			=> 'danslokalen_feedback',
    'meta_key'   		=> $field,
    'meta_value' 		=> $id,
    'meta_compare' 	=> 'LIKE',
		'post_status'		=> 'draft'
	) );

	return $post_ids;

}

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {

	// $fields['billing']['billing_address_1']['class'][0]	 	= 'col-6';
	// $fields['billing']['billing_address_2']['class'][0] 	= 'col-6';
	// $fields['billing']['billing_address_2']['label']	 		= '&nbsp;';
	//
	// $fields['order']['order_comments']['class'][0] 			= 'col-12';

	return $fields;
}

function embed_spotify_link( $link ) {
	$retlink = $link;
	$retlink = str_replace("/artist/", "/embed/artist/", $retlink);
	$retlink = str_replace("/playlist/", "/embed/playlist/", $retlink);

	return $retlink;
}

function return_your_product_ids( $antal = null, $orderby = "id", $order = "ASC" ) {

  if ( !$antal ) {
    $antal = -1;
  }

  $customer_orders = get_posts( array(
    'numberposts' => $antal,
    'orderby'     => $orderby,
    'order'       => $order,
    'meta_key'    => '_customer_user',
    'meta_value'  => get_current_user_id(),
    'post_type'   => wc_get_order_types(),
    'post_status' => array_keys( wc_get_is_paid_statuses() ),
  ) );

  if ( ! $customer_orders ) return;
  $product_ids = array();
  foreach ( $customer_orders as $customer_order ) {
      $order = wc_get_order( $customer_order->ID );
      $items = $order->get_items();
      foreach ( $items as $item ) {
          $product_id = $item->get_product_id();
          $product_ids[] = $product_id;
      }
  }
  $product_ids = array_unique( $product_ids );
  $product_ids_str = implode( ",", $product_ids );

  return $product_ids_str;

}

/**
 * @snippet       WooCommerce Add New Tab @ My Account
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 3.5.7
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */

// ------------------
// 1. Register new endpoint to use for My Account page
// Note: Resave Permalinks or it will give 404 error

function bbloomer_add_premium_support_endpoint() {
    add_rewrite_endpoint( 'kopt-material', EP_ROOT | EP_PAGES );
}

add_action( 'init', 'bbloomer_add_premium_support_endpoint' );


// ------------------
// 2. Add new query var

function bbloomer_premium_support_query_vars( $vars ) {
    $vars[] = 'kopt-material';
    return $vars;
}

add_filter( 'query_vars', 'bbloomer_premium_support_query_vars', 0 );


// ------------------
// 3. Insert the new endpoint into the My Account menu

function bbloomer_add_premium_support_link_my_account( $items ) {
    $items['kopt-material'] = 'Köpt material';
    return $items;
}

add_filter( 'woocommerce_account_menu_items', 'bbloomer_add_premium_support_link_my_account' );


// ------------------
// 4. Add content to the new endpoint

function bbloomer_premium_support_content() {
echo '<h3>Köpt material</h3>';
$product_ids_str = return_your_product_ids();
echo do_shortcode( "[products columns='3' ids='$product_ids_str']" );
}

add_action( 'woocommerce_account_kopt-material_endpoint', 'bbloomer_premium_support_content' );
// Note: add_action must follow 'woocommerce_account_{your-endpoint-slug}_endpoint' format

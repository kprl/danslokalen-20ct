<div class="card-columns">

  <?php
  foreach ($dansbandArr as $key => $dansband):
    $ubidkey = $block['id'] . "-" . $key;
  ?>

    <div class="card">
      <img src="<?php echo get_the_post_thumbnail_url($dansband['ID'], 'medium'); ?>" class="card-img-top" alt="<?php echo $dansband['title']; ?>">
      <div class="card-body">
        <h3 class="card-title"><?php echo $dansband['title']; ?></h3>
        <div class="card-text">
          <div class="danslokalen_dansband-contact">
            <?php
            if ( is_array($dansband['contact']) ):
              echo '<ul class="list-unstyled my-4">';
              foreach ($dansband['contact'] as $key => $contact) {
                ?>

                <li class="media">
                  <div class="media-svg mr-3">
                    <?php echo $contact['svg']; ?>
                  </div>
                  <div class="media-body">
                    <a href="<?php echo $contact['value']['link']; ?>" target="<?php echo $contact['value']['target']; ?>" class="<?php echo $contact['class']; ?>">
                      <?php echo $contact['value']['display']; ?>
                    </a>
                  </div>
                </li>

                <?php
              }
              echo '</ul>';
              if ( array_key_exists('embed', $dansband['contact']['spotify']['value'] ) ) {
                echo '<iframe src="' . $dansband['contact']['spotify']['value']['embed'] . '" width="300" height="180" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>';
              }
            endif;
            ?>
          </div>

        </div>
      </div>
    </div>

  <?php endforeach; ?>

</div>

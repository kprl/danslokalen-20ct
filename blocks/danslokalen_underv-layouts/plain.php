<?php
  if ( !is_user_logged_in() ) {
    echo "<h4 class='ingress' style='text-align:center;'>Du måste logga in för att lista dina produkter!</h4>";
    echo do_shortcode("[woocommerce_my_account]");
  } else if ( !$product_ids_str ) {
    echo "<h4 class='ingress' style='text-align:center;'>Du har ännu inte köpt några produkter!</h4>";
    echo do_shortcode("[products category='kursmaterial' ]");
  } else {
    echo do_shortcode("[products ids='$product_ids_str']");
  }
?>

<?php
/**
 * Block Name: Danslokalen Dansband
 */

// get image field (array)
$fields = get_fields();

if( $fields ):

  $id = 'danslokalen_underv-' . $block['id'];

  $align_class  = $block['align'] ? 'align' . $block['align'] : '';
  if (in_array('className', $block)) {
    $css_class  = $block['className'];
  } else {
    $css_class  = '';
  }

  $product_ids_str = return_your_product_ids( $fields['antal'], $fields['ordning'], $fields['sortering'] );

  $layout_folder = get_stylesheet_directory() . '/blocks/danslokalen_underv-layouts/';

?>

  <div id="<?php echo $id; ?>" class="danslokalen_underv <?php echo $align_class; ?> <?php echo $css_class; ?>">

    <?php
      if ( is_admin() ):
        include $layout_folder . 'admin.php';
      else:

        include $layout_folder . $fields['danslokalen_underv-layout'] . '.php';

      endif;
    ?>

  </div>

  <?php

endif;

<?php
/**
 * Block Name: Danslokalen Dansband
 */

// get image field (array)
$fields = get_fields();

if( $fields ):

  $id = 'danslokalen_dansband-' . $block['id'];

  $align_class  = $block['align'] ? 'align' . $block['align'] : '';
  if (in_array('className', $block)) {
    $css_class  = $block['className'];
  } else {
    $css_class  = '';
  }

  if ( $fields['danslokalen_dansband-content'] == "all-dansband" ) {
    $content = null;
  } else if ( $fields['danslokalen_dansband-content'] == "specific-dansband" ) {
    $content = $fields['danslokalen_dansband-specific-dansband'];
  }

  $dansbandArr = return_danslokalen_dansband( $fields['danslokalen_dansband-content'], $content, $fields['antal'], $fields['ordning'], $fields['sortering'], $fields['visa_spotify-embed'] );

  $layout_folder = get_stylesheet_directory() . '/blocks/danslokalen_dansband-layouts/';

?>

  <div id="<?php echo $id; ?>" class="danslokalen_dansband <?php echo $align_class; ?> <?php echo $css_class; ?>">

    <?php
      if ( is_admin() ):
        include $layout_folder . 'admin.php';
      else:

        // plain
        // card-horizontal
        // card-group
        // card-deck
        // card-columns
        // contact-expand

        include $layout_folder . $fields['danslokalen_dansband-layout'] . '.php';

      endif;
    ?>

  </div>

  <?php

endif;

function return_contact_array_2 ( $key = false ) {

  //webpage
  $contactArray['webpage']['key']             = 'webpage';
  $contactArray['webpage']['title']           = 'Webbplats';
  $contactArray['webpage']['pre_username']    = '';
  $contactArray['webpage']['class']           = 'webpage';
  $contactArray['webpage']['color']           = '#2c3e50';
  $contactArray['webpage']['svg']             = '<svg viewBox="0 0 32 32" class="icon icon-externalLink" viewBox="0 0 32 32" aria-hidden="true"><path d="M23.5 23.5h-15v-15h4.791V6H6v20h20v-7.969h-2.5z"/><path d="M17.979 6l3.016 3.018-6.829 6.829 1.988 1.987 6.83-6.828L26 14.02V6z"/></svg>';

  //spotify
  $contactArray['spotify']['key']              = 'spotify';
  $contactArray['spotify']['title']            = 'Spotify';
  $contactArray['spotify']['pre_username']     = '';
  $contactArray['spotify']['class']            = 'spotify';
  $contactArray['spotify']['color']            = '#1DB954';
  $contactArray['spotify']['svg']              = '<svg viewBox="0 0 32 32" class="icon icon-play" viewBox="0 0 32 32" aria-hidden="true"><path d="M10.968 23V9l12.762 7-12.762 7z"/></svg>';

  if ( $key ) {
    return $contactArray[$key];
  } else {
    return $contactArray;
  }

}

function return_danslokalen_dansband( $content = null, $value = null, $antal = null, $orderby = "id", $order = "ASC", $embed = true ) {

  if ( $content == "specific-dansband" ) {
    foreach ($value as $key => $v) {
      $banden[] = $v->ID;
    }
  } else {
    $banden = array();
  }

  if ( !$antal ) {
    $antal = -1;
  }

  $dbndArr = get_posts(array(
    'post_type' 		  => 'danslokalen_dansband',
    'post__in'        => $banden,
    'posts_per_page'  => $antal,
    'orderby'         => $orderby,
    'order'           => $order,
    'status'          => 'published',
  ));

  foreach ($dbndArr as $key => $dbnd) {
    $dansbandArr[$key]['ID']            = $dbnd->ID;
    $dansbandArr[$key]['title']         = $dbnd->post_title;
    $dansbandArr[$key]['content']       = wpautop( $dbnd->post_content );
    $dansbandArr[$key]['thumbnail_id']  = get_post_thumbnail_id( $dbnd->ID );
    $dansbandArr[$key]['contact']       = null;

    //$args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
    $args = array();
    $fields = get_fields($dbnd->ID);

    if ( is_array($fields['kontaktinformation']) ) {

      $contactArray = return_contact_array();
      $contactArray = $contactArray + return_contact_array_2();
      foreach ($fields['kontaktinformation'] as $c) {

        $layout = $c['acf_fc_layout'];
        $ckey   = $c['acf_fc_layout'];
        $dansbandArr[$key]['contact'][$ckey]['key']           = $layout;
        $dansbandArr[$key]['contact'][$ckey]['title']         = $contactArray[$layout]['title'];
        $dansbandArr[$key]['contact'][$ckey]['pre_username']  = $contactArray[$layout]['svg'];
        $dansbandArr[$key]['contact'][$ckey]['class']         = $contactArray[$layout]['class'];
        $dansbandArr[$key]['contact'][$ckey]['color']         = $contactArray[$layout]['color'];
        $dansbandArr[$key]['contact'][$ckey]['svg']           = $contactArray[$layout]['svg'];

        if ( $layout == 'facebook' OR $layout == 'instagram' OR $layout == 'twitter' OR $layout == 'youtube' OR $layout == 'skype' OR $layout == 'github' OR $layout == 'bitbucket' ) {
          $dansbandArr[$key]['contact'][$ckey]['value']['link']     = $c['link']['url'];
          $dansbandArr[$key]['contact'][$ckey]['value']['target']   = "_blank";//$c['link']['target'];
          $dansbandArr[$key]['contact'][$ckey]['value']['display']  = $contactArray[$layout]['pre_username'] . $c['username'];
        } else if ( $layout == 'webpage' ) {
          $dansbandArr[$key]['contact'][$ckey]['value']['link']     = $c['link']['url'];
          $dansbandArr[$key]['contact'][$ckey]['value']['target']   = "_blank";//$c['link']['target'];
          $dansbandArr[$key]['contact'][$ckey]['value']['display']  = $c['title'];
        } else if ( $layout == 'spotify' ) {
          $dansbandArr[$key]['contact'][$ckey]['value']['link']     = $c['link']['url'];
          $dansbandArr[$key]['contact'][$ckey]['value']['target']   = "_blank";//$c['link']['target'];
          $dansbandArr[$key]['contact'][$ckey]['value']['display']  = $c['title'];
          if ( $embed ) {
            $dansbandArr[$key]['contact'][$ckey]['value']['embed']  = embed_spotify_link( $c['link']['url'] );
          }
        } else if ( $layout == 'phone' ) {
          $dansbandArr[$key]['contact'][$ckey]['value']['link']     = "tel:" . $c['phonenumber'];
          $dansbandArr[$key]['contact'][$ckey]['value']['target']   = null;
          $dansbandArr[$key]['contact'][$ckey]['value']['number']   = $c['phonenumber'];

          $nummer = $c['phonenumber'];
          $nummer = str_replace("+46", "0", $nummer);
          $nummer = substr_replace($nummer, '-', 3, 0);
          $nummer = substr_replace($nummer, ' ', 7, 0);
          $nummer = substr_replace($nummer, ' ', 10, 0);

          $dansbandArr[$key]['contact'][$ckey]['value']['display']  = $nummer;
        } else if ( $layout == 'email' ) {
          $dansbandArr[$key]['contact'][$ckey]['value']['link']     = "mailto:" . $c['email'];
          $dansbandArr[$key]['contact'][$ckey]['value']['target']   = null;
          $dansbandArr[$key]['contact'][$ckey]['value']['number']   = $c['email'];
          $dansbandArr[$key]['contact'][$ckey]['value']['display']  = $c['email'];
        }

      }
    }

    $contactArray = array();
    $fields = array();
  }

  return $dansbandArr;

}

<?php

if ( ! function_exists('danslokalen_underv_cpt') ) {

  // Register Custom Post Type
  function danslokalen_underv_cpt() {

  	$labels = array(
      'name'                  => _x( 'Undervisningar', 'Post Type General Name', 'twentytwenty' ),
  		'singular_name'         => _x( 'Undervisning', 'Post Type Singular Name', 'twentytwenty' ),
  		'menu_name'             => __( 'Undervisning', 'twentytwenty' ),
  		'name_admin_bar'        => __( 'Undervisning', 'twentytwenty' ),
  		'all_items'             => __( 'Alla undervisningar', 'twentytwenty' ),
  		'add_new_item'          => __( 'Lägg till ny undervisning', 'twentytwenty' ),
  		'add_new'               => __( 'Lägg till', 'twentytwenty' ),
  		'new_item'              => __( 'Ny undervisning', 'twentytwenty' ),
  		'edit_item'             => __( 'Redigera undervisning', 'twentytwenty' ),
  		'update_item'           => __( 'Uppdatera undervisning', 'twentytwenty' ),
  		'view_item'             => __( 'Visa undervisning', 'twentytwenty' ),
  		'view_items'            => __( 'Visa undervisningar', 'twentytwenty' ),
  		'search_items'          => __( 'Sök undervisning', 'twentytwenty' ),
  	);

    $rewrite = array(
  		'slug'                => 'undervisning',
  	);

  	$args = array(
  		'label'                 => __( 'Undervisning', 'twentytwenty' ),
  		'description'           => __( 'Danslokalens undervisningar', 'twentytwenty' ),
  		'labels'                => $labels,
  		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'post-formats' ),
  		'taxonomies'            => array( 'category', 'post_tag' ),
  		'hierarchical'          => true,
  		'public'                => true,
  		'show_ui'               => true,
  		'show_in_menu'          => 'danslokalen-options',
      'show_in_rest'          => true,
  		'menu_position'         => 5,
  		'show_in_admin_bar'     => true,
  		'show_in_nav_menus'     => true,
  		'can_export'            => true,
  		'has_archive'           => false,
  		'exclude_from_search'   => true,
  		'publicly_queryable'    => true,
  		'capability_type'       => 'page',
      'rewrite'               => $rewrite,
  	);
  	register_post_type( 'danslokalen_underv', $args );

  }
  add_action( 'init', 'danslokalen_underv_cpt', 0 );

}

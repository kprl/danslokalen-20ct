<?php

if ( ! function_exists('danslokalen_feedback_cpt') ) {

  // Register Custom Post Type
  function danslokalen_feedback_cpt() {

  	$labels = array(
  		'name'                  => _x( 'Feedback', 'Post Type General Name', 'twentytwenty' ),
  		'singular_name'         => _x( 'Feedback', 'Post Type Singular Name', 'twentytwenty' ),
  		'menu_name'             => __( 'Feedback', 'twentytwenty' ),
  		'name_admin_bar'        => __( 'Feedback', 'twentytwenty' ),
  		'all_items'             => __( 'Alla feedback', 'twentytwenty' ),
      'add_new_item'          => __( 'Lägg till feedback', 'twentytwenty' ),
  		'add_new'               => __( 'Lägg till', 'twentytwenty' ),
  		'new_item'              => __( 'Nytt feedback', 'twentytwenty' ),
  		'edit_item'             => __( 'Redigera feedback', 'twentytwenty' ),
  		'update_item'           => __( 'Uppdatera feedback', 'twentytwenty' ),
  		'view_item'             => __( 'Visa feedback', 'twentytwenty' ),
  		'view_items'            => __( 'Visa feedback', 'twentytwenty' ),
  		'search_items'          => __( 'Sök feedback', 'twentytwenty' ),
  	);
  	$args = array(
  		'label'                 => __( 'Feedback', 'twentytwenty' ),
  		'description'           => __( 'Feedback för deltagare', 'twentytwenty' ),
  		'labels'                => $labels,
  		'supports'              => array( 'title', 'editor', 'author' ),
  		'taxonomies'            => array( 'category', 'post_tag' ),
  		'hierarchical'          => false,
  		'public'                => true,
      'show_ui'               => true,
  		'show_in_menu'          => 'danslokalen-options',
      'show_in_rest'          => true,
  		'menu_position'         => 5,
  		'show_in_admin_bar'     => false,
  		'show_in_nav_menus'     => true,
  		'can_export'            => true,
  		'has_archive'           => false,
  		'exclude_from_search'   => true,
  		'publicly_queryable'    => true,
  		'capability_type'       => 'page',
  	);
  	register_post_type( 'danslokalen_feedback', $args );

  }
  add_action( 'init', 'danslokalen_feedback_cpt', 0 );

}

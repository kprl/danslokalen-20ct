<?php

if ( ! function_exists('danslokalen_dansband_cpt') ) {

  // Register Custom Post Type
  function danslokalen_dansband_cpt() {

  	$labels = array(
  		'name'                  => _x( 'Dansband', 'Post Type General Name', 'twentytwenty' ),
  		'singular_name'         => _x( 'Dansband', 'Post Type Singular Name', 'twentytwenty' ),
  		'menu_name'             => __( 'Dansband', 'twentytwenty' ),
  		'name_admin_bar'        => __( 'Dansband', 'twentytwenty' ),
  		'all_items'             => __( 'Alla dansband', 'twentytwenty' ),
  		'add_new_item'          => __( 'Lägg till nytt dansband', 'twentytwenty' ),
  		'add_new'               => __( 'Lägg till', 'twentytwenty' ),
  		'new_item'              => __( 'Nytt dansband', 'twentytwenty' ),
  		'edit_item'             => __( 'Redigera dansband', 'twentytwenty' ),
  		'update_item'           => __( 'Uppdatera dansband', 'twentytwenty' ),
  		'view_item'             => __( 'Visa dansband', 'twentytwenty' ),
  		'view_items'            => __( 'Visa dansband', 'twentytwenty' ),
  		'search_items'          => __( 'Sök dansband', 'twentytwenty' ),
  	);

    $rewrite = array(
  		'slug'                => 'dansbandet',
  	);

  	$args = array(
  		'label'                 => __( 'Dansband', 'twentytwenty' ),
  		'description'           => __( 'Dansband i samarbete', 'twentytwenty' ),
  		'labels'                => $labels,
  		'supports'              => array( 'title', 'editor', 'thumbnail' ),
  		'taxonomies'            => array( 'category', 'post_tag' ),
  		'hierarchical'          => false,
  		'public'                => true,
  		'show_ui'               => true,
  		'show_in_menu'          => 'danslokalen-options',
      'show_in_rest'          => true,
  		'menu_position'         => 5,
  		'show_in_admin_bar'     => true,
  		'show_in_nav_menus'     => true,
  		'can_export'            => true,
  		'has_archive'           => false,
  		'exclude_from_search'   => true,
  		'publicly_queryable'    => true,
  		'capability_type'       => 'page',
      'rewrite'               => $rewrite,
  	);
  	register_post_type( 'danslokalen_dansband', $args );

  }
  add_action( 'init', 'danslokalen_dansband_cpt', 0 );

}
